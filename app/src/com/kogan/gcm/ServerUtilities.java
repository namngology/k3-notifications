
package com.kogan.gcm;

import static com.kogan.gcm.core.Settings.REGISTER_URL;
import static com.kogan.gcm.core.Settings.TAG;

import android.content.Context;
import android.util.Log;

import com.google.android.gcm.GCMRegistrar;
import com.github.kevinsawicki.http.HttpRequest;

public final class ServerUtilities {

    /**
     * Register this account/device pair within the server.
     *
     * @return whether the registration succeeded or not.
     */
    static boolean register(final Context context, final String regId) {
        Log.i(TAG, "registering device (regId = " + regId + ")");
        int status_code = HttpRequest.post(REGISTER_URL).send("registration_id=" + regId).code();
        if( status_code == 200){
        	GCMRegistrar.setRegisteredOnServer(context, true);
        	return true;
        }
        return false;
    }

    /**
     * Unregister this account/device pair within the server.
     */
    static void unregister(final Context context, final String regId) {
        Log.i(TAG, "unregistering device (regId = " + regId + ")");
        String body = "registration_id=" + regId + "&unregister=True";
    	HttpRequest.post(REGISTER_URL).send(body).code();
        GCMRegistrar.setRegisteredOnServer(context, false);
    }

}
