package com.kogan.gcm;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MobileViewActivity extends Activity{
	WebView engine;
 
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.mobile);
        
        engine = (WebView) findViewById(R.id.web_engine);
        
        // Follow redirects
        engine.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                view.loadUrl(url);
                return false; // then it is not handled by default action
           }
        });
        
        Bundle b = getIntent().getExtras();
        String mobile_url = null;
        if(b == null){
        	mobile_url =  "https://m.kogan.com/";
        }
        else{
        	mobile_url = b.getString("mobile_url");
        }

        engine.loadUrl(mobile_url);
        engine.getSettings().setJavaScriptEnabled(true);
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && engine.canGoBack()) {
            engine.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }
}
