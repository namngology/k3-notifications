package com.kogan.gcm.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class EventData {
	private String url;
	private String mobile_url;
	private String image_url;
	private String thumbnail;
	private String title;
	
	public EventData(String url, String image_url, String thumbnail,
			String title) {
		super();
		this.url = url;
		this.image_url = image_url;
		this.thumbnail = thumbnail;
		this.title = title;
	}
	
	public String getUrl() {
		return url;
	}
	public String getMobileUrl() {
		return mobile_url;
	}
	public String getImageUrl() {
		return image_url;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public String getTitle() {
		return title;
	}
	
    public Bitmap getBitmapFromImageURL() {
        try {
            URL url = new URL(image_url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
