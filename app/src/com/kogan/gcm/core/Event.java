package com.kogan.gcm.core;

import com.kogan.gcm.MobileViewActivity;
import com.kogan.gcm.R;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;



public class Event {
	private String type;
	private String message;
	private EventData data;
	
	
	public Event(String type, String message, EventData data) {
		super();
		this.type = type;
		this.message = message;
		this.data = data;
	}
	
	public String getType() {
		return type;
	}
	public String getMessage() {
		return message;
	}
	public EventData getData() {
		return data;
	}

	public Notification buildNotification(Context context){
		Bitmap image = getData().getBitmapFromImageURL();
		if(data.getUrl() == null){
			return buildNonProductNotification(context);
		}
		
		Intent mobileIntent = new Intent(context, MobileViewActivity.class);
		mobileIntent.putExtra("mobile_url", data.getMobileUrl());
		
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, data.getUrl());
		sendIntent.setType("text/plain");

		PendingIntent pMobileIntent = PendingIntent.getActivity(context, 0, mobileIntent, 0);
		PendingIntent pSendIntent = PendingIntent.getActivity(context, 0, sendIntent, 0);
		
		NotificationCompat.Builder build = new NotificationCompat.Builder(context)
			.setContentTitle(data.getTitle())
			.setSmallIcon(R.drawable.ic_launcher)
			.addAction(R.drawable.navigation_accept, "Browse", pMobileIntent)
			.addAction(R.drawable.social_send, "Share", pSendIntent);

		Notification notification = new NotificationCompat.BigPictureStyle(build)
			.bigPicture(image)
			.setSummaryText(this.getMessage())
			.build();

		return notification;
	}
	
	public Notification buildNonProductNotification(Context context){
		Intent mobileIntent = new Intent(context, MobileViewActivity.class);
		PendingIntent pMobileIntent = PendingIntent.getActivity(context, 0, mobileIntent, 0);
		
		NotificationCompat.Builder build = new NotificationCompat.Builder(context)
			.setContentTitle(data.getTitle())
			.setSmallIcon(R.drawable.ic_launcher)
			.addAction(R.drawable.navigation_accept, "Browse", pMobileIntent);
		
		Notification notification = new NotificationCompat.BigTextStyle(build)
			.bigText(getMessage())
			.build();
		
		return notification;
	}

}
