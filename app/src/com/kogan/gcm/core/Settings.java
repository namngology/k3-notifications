
package com.kogan.gcm.core;


public final class Settings {
	public static final String DOMAIN = "http://192.168.1.57:8000";
    public static final String APP_URL = DOMAIN + "/au/events";
    public static final String REGISTER_URL = APP_URL + "/register_device/";
    public static final String SENDER_ID = "220504711102";
    public static final String TAG = "KOGAN";
}
